'use strict';

function crear_roles() {
    var rol = require('../models/rol');
    rol.run().then(function(roles) {
        if (roles.length <= 0) {
            rol.save([
                { nombre: "Administrador" },
                { nombre: "Usuario" }
            ]);
        }

    }).error(function(error) {
        //console.log(error);
        res.send(error);
    });
}

function crearCategorias() {
    var categoria = require('../models/categoria');
    categoria.run().then(function(categorias) {
        if (categorias.length <= 0) {

            categoria.save([{
                    nombre: "Murales",
                    estado: true
                },
                {
                    nombre: "Hoteless",
                    estado: true
                },
                {
                    nombre: "Iglesias",
                    estado: true
                },
                {
                    nombre: "Hospitales",
                    estado: false
                }
            ]);
        }

    }).error(function(error) {
        res.send(error);
    });
}

function validaLogin(req) {
    return (req.session !== undefined && req.session.usuario !== undefined);
}
module.exports = { crear_roles, validaLogin, crearCategorias };