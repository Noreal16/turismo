'use strict';
var categoria = require('../models/categoria');
var sitio = require('../models/sitios');
class sitioControl {

    visualizar(req, res) {
        sitio.getJoin({ categoria: true }).then(function(lista) {
            console.log(lista);
            var msgListador = {
                error: req.flash("error"),
                info: req.flash("info")
            };
            res.render("index", {
                title: "Listado de Sitios",
                sesion: true,
                fragmento: 'fragmentos/sitios/lista',
                msg: msgListador,
                lista: lista
            });
        }).error(function(error) {
            req.flash('error', 'Existe un error por favor comunicarse con el desarrollador del sitio!');
            res.redirect('/')
        });


    }

    visualizarLista(req, res) {
        categoria.filter({ estado: true }).then(function(lista) {
            var msgListador = {
                error: req.flash("error"),
                info: req.flash("info")
            };
            res.render("index", {
                title: "Registro de Sitios",
                sesion: true,
                fragmento: 'fragmentos/sitios/registroS',
                msg: msgListador,
                lista: lista
            });

        }).error(function(error) {
            req.flash('error', 'Existe un error por favor comunicarse con el desarrollador del sitio!');
            res.redirect('/sitios')
        });

    }

    guardar(req, res) {
        categoria.filter({ external_id: req.body.categoria }).then(function(cat) {
            if (cat.length > 0) {
                var dataS = {
                    nombre: req.body.nombre,
                    descripcion: req.body.descripcion,
                    estado: true,
                    id_categoria: cat[0].id
                };
                var sitioS = new sitio(dataS);
                sitioS.save().then(function(sitioSave) {
                    req.flash('info', 'Se guardo el sitio correctamente!');
                    res.redirect('/sitios');
                }).error(function(error) {
                    req.flash('error', 'Error del servidor por favor comunicarse con el desarrollador!');
                    res.redirect('/')
                });
            } else {
                req.flash('error', 'Por favor seleccione una categoria!');
                res.redirect('/Registro_Sitios')
            }


        }).error(function(error) {
            req.flash('error', 'Existe un error por favor comunicarse con el desarrollador del sitio!');
            res.redirect('/sitios');
        });
    }


}
module.exports = sitioControl;