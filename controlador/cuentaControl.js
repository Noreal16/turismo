'use strict';
//var persona = require('../models/persona');
var cuenta = require('../models/cuenta');
var validar = require('./roles_control');
class cuentaControl {

    visualizar(req, res) {
        if (!validar.validaLogin(req)) {
            var msgListado = { error: req.flash("error"), info: req.flash("info") };
            res.render("index", { title: "Registrate", sesion: true, fragmento: 'fragmentos/registro', msg: msgListado });

        } else {
            res.redirect('/');
        }
    }
    visualizarSesion(req, res) {
        if (!validar.validaLogin(req)) {
            var msgListado = { error: req.flash("error"), info: req.flash("info") };
            res.render("index", { title: "Iniciar Sesion", sesion: true, fragmento: 'fragmentos/sesion', msg: msgListado });

        } else {
            res.redirect('/');
        }
    }
    inicio_sesion(req, res) {
        cuenta.getJoin({ persona: { rol: true } }).filter({ correo: req.body.correo }).run().then(function(cuent) {
            if (cuent.length > 0) {
                var acount = cuent[0];
                if (acount.clave === req.body.clave) {
                    req.session.usuario = {
                        external: acount.persona.external_id,
                        rol: acount.persona.rol.nombre,
                        nombre: acount.persona.apellidos + " " + acount.persona.nombre
                    }
                    res.redirect('/')
                        //res.render("index", { title: "Iniciar Sesion", sesion: req.session.usuario, fragmento: 'fragmentos/principal' });

                    //res.send(req.session.usuario);
                } else {
                    req.flash('error', 'Sus credenciales no son validas!');
                    res.redirect('/inicio_sesion');
                }
            } else {
                req.flash('error', 'Sus credenciales no son validas!');
                res.redirect('/inicio_sesion');
            }
        }).error(function(error) {
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema !');
            res.redirect('/registrarse');
        });
    }
    cerrar_sesion(req, res) {
        req.session.destroy();
        res.redirect('/');
    }

}
module.exports = cuentaControl;