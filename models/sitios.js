var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Sitios = thinky.createModel("Sitios", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    nombre: type.string(),
    descripcion: type.string(),
    estado: type.boolean(),
    id_categoria: type.string()
});
module.exports = Sitios;
var Categoria = require('./categoria');
Sitios.belongsTo(Categoria, "categoria", "id_categoria", "id");