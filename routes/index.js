var express = require('express');
var router = express.Router();
var registro = require('../controlador/cuentaControl');
var registroP = new registro();
var utilidades = require('../controlador/roles_control');
var personaCuenta = require('../controlador/persona_controler');
var personCuen = new personaCuenta();
var sitios = require('../controlador/sitioControl');
var sitC = new sitios();

/**
 * midword o cortafuegos
 */
var auth = function(req, res, next) {
        if (!utilidades.validaLogin(req)) {
            req.flash('error', 'Debe iniciar sesion!');
            res.redirect('/inicio_sesion');
        } else {
            next();
        }
    }
    /* Para inicio de sesion.
    en el if se controla el inicio de sesion 
    */
router.get('/', function(req, res, next) {
    utilidades.crear_roles();
    utilidades.crearCategorias();
    if (!utilidades.validaLogin(req)) {
        res.render('index', {
            title: 'Express',
            sesion: false
        });
    } else {
        res.render("index", {
            title: "Principal",
            sesion: true,
            usuario: req.session.usuario,
            fragmento: 'fragmentos/principal'
        });
    }

});
/**Registro e Inicio de sesion */
router.get('/registrarse', registroP.visualizar);
/**Para crear roles actumaticamente */
utilidades.crear_roles();
/**llamar con post a la funcion gurardar */
router.post('/registrarse', personCuen.guardar);
/**Llamar al inicio de sesion */
router.get('/inicio_sesion', registroP.visualizarSesion);
/**inisio sesion */
router.post('/inicio_sesion', registroP.inicio_sesion);
/**Para cerrar sesion */
router.get('/cerrar_sesion', auth, registroP.cerrar_sesion);
/**llamar a sitios */
router.get('/sitios', auth, sitC.visualizar);
/**LLamar sitios desde ventana tabla  */
router.get('/Registro_Sitios', auth, sitC.visualizarLista);
/**Llamar a guardar sitio turistico */
router.post('/Registro_Sitios', auth, sitC.guardar);
module.exports = router;