var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Categoria = thinky.createModel("Categoria", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    nombre: type.string(),
    estado: type.boolean()
});
module.exports = Categoria;
var Sitios = require('./sitios');
Categoria.hasMany(Sitios, "sitios", "id", "id_categoria");