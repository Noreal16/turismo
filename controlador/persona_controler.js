'use strict';
var rol = require('../models/rol');
var persona = require('../models/persona');
var cuenta = require('../models/cuenta');
class persona_controler {
    guardar(req, res) {
        rol.filter({ nombre: "Usuario" }).run().then(function(roles) {

            if (roles.length > 0) {
                cuenta.filter({ correo: req.body.correo }).run().then(function(existe) {
                    if (existe.length <= 0) {
                        //guardar
                        var role = roles[0];
                        var dataP = {
                            apellidos: req.body.apellido,
                            nombre: req.body.nombre,
                            fecha_nac: req.body.fecha,
                            edad: req.body.edad,
                            id_rol: role.id
                        };
                        var personaP = new persona(dataP);
                        var dataC = {
                            correo: req.body.correo,
                            clave: req.body.clave
                        };
                        var cuentaP = new cuenta(dataC);
                        personaP.cuenta = cuentaP;
                        personaP.saveAll({ cuenta: true }).then(function(personaSave) {
                            req.flash('info', 'se ha guardado correctamente!');
                            res.redirect('/inicio_sesion');

                            //res.send(personaSave);
                        }).error(function(errores) {
                            //console.log(errores);
                            //res.send(errores);
                            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema !');
                            res.redirect('/');
                        });
                    } else {
                        req.flash('error', 'El correo ingresado  ya esta registrado!');
                        res.redirect('/registrarse');
                    }
                }).error(function(error) {
                    req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema !');
                    res.redirect('/registrarse');
                });

            } else {
                //res.send("No existen Roles");
                req.flash('error', 'No existen roles registrados !');
                res.redirect('/registrarse');
                res.redirect('/');
            }
        }).error(function(error) {
            //console.log(error);
            //res.send(error);
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema !');
            res.redirect('/registrarse');

        });
    }
}
module.exports = persona_controler;